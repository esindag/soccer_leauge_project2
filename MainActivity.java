package com.example.soccerleauge;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {


  Button button9, button10 ;
  EditText name;
  ListView footballreport;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //
        button9 = findViewById(R.id.button9);
        button10 = findViewById(R.id.button10);




 //listeners


        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent button10 = new Intent(MainActivity.this,FiksturPage.class);
                startActivity(button10);
            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Instantiate the RequestQueue.
                RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
                String url ="https://run.mocky.io/v3/81121ef6-3f96-4a3c-bb0e-6d10444b0e4b ";

                JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        String TeamID = "" ;

                        try {
                            JSONObject teaminfo = response.getJSONObject(0) ;
                                TeamID = teaminfo.getString("title");


                        } catch (JSONException e ) {
                            e.printStackTrace();
                        }

                        Toast.makeText(MainActivity.this, "Team ID" + TeamID, Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Bir şeyler yanlış", Toast.LENGTH_SHORT).show();

                    }
                }) ;

                queue.add(request);


                button10.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent button10 = new Intent(MainActivity.this,FiksturPage.class);
                        startActivity(button10);
                    }
                });

//// Request a string response from the provided URL.
//                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
//                        new Response.Listener<String>() {
//                            @Override
//                            public void onResponse(String response) {
//
//                                Toast.makeText(MainActivity.this, response, Toast.LENGTH_SHORT).show();
//                            }
//                        }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(MainActivity.this, "Hata", Toast.LENGTH_SHORT).show();
//                    }
//                });

// Add the request to the RequestQueue.


               // Toast.makeText(MainActivity.this, " Takım ismi " + name.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}